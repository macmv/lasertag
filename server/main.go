package main

import (
  "net"
  "log"
  "context"
  "math/rand"

  "google.golang.org/grpc"

  pb "gitlab.com/macmv/lasertag/proto"
)

func main() {
  host := ":3458"
  l, err := net.Listen("tcp", host)

  if err != nil {
    log.Fatalf("Failed to open port: %v", err)
  }

  grpc_server := grpc.NewServer()
  pb.RegisterConnServer(grpc_server, &server{
    players: make(map[int32]player_info),
  })

  log.Println("Starting server on", host)
  grpc_server.Serve(l)
}

type player_info struct {
  id int32
  team int32
  name string
  lat float64
  lon float64
  c context.Context
}

type server struct {
  // Must embed this for some reason
  pb.UnimplementedConnServer

  players map[int32]player_info
}

func (s *server) GetInfo() (*pb.GameInfo) {
  game := pb.GameInfo{}
  for _, info := range s.players {
    game.Players = append(game.Players, &pb.PlayerInfo{
      Id: info.id,
      Team: info.team,
      Name: info.name,
    })
  }
  return &game
}

func (s *server) JoinGame(c context.Context, req *pb.JoinRequest) (*pb.GameInfo, error) {
  log.Println("Got join game from client")
  info := player_info{
    id: rand.Int31(),
    team: req.Team,
    name: req.Name,
    c: c,
  }
  s.players[info.id] = info
  game_info := s.GetInfo()
  game_info.Id = info.id
  return game_info, nil
}

func (s *server) ChangeInfo(c context.Context, req *pb.ChangeInfoRequest) (*pb.GameInfo, error) {
  log.Println("Got change info from client")
  game_info := s.GetInfo()
  game_info.Id = req.Id
  return game_info, nil
}

func (s *server) UpdatePosition(c context.Context, req *pb.MapPosition) (*pb.PlayerPositions, error) {
  log.Println("Got update position from client")
  return nil, nil
}
