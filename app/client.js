import { NativeModules } from 'react-native';

const { ConnectionModule } = NativeModules;

export function joinGame() {
  console.log("Joining game");
  ConnectionModule.connect("192.168.4.169", 3458);
  ConnectionModule.joinGame("macmv", 1, (id, info) => {
    console.log("Got id:", id);
    console.log("Got info:", info);
  });
}

export function changeInfo(name) {
  console.log("Changing info");
  ConnectionModule.changeInfo(name, 1, info => {
    console.log("Got info:", info);
  });
}
