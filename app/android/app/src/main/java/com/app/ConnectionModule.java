package com.app;

import android.util.Log;

import androidx.annotation.RequiresPermission;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableNativeMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class ConnectionModule extends ReactContextBaseJavaModule {
  private ConnGrpc.ConnBlockingStub stub;

  ConnectionModule(ReactApplicationContext context) {
    super(context);
  }

  @ReactMethod
  public void connect(String host, int port) {
    ManagedChannel chan = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
    stub = ConnGrpc.newBlockingStub(chan);
  }

  @ReactMethod
  public void joinGame(String name, int team, Callback cb) {
    Connection.JoinRequest req = Connection.JoinRequest.newBuilder()
      .setName(name)
      .setTeam(team)
      .build();
    Connection.GameInfo info = stub.joinGame(req);

    cb.invoke(info.getId(), convertPlayers(info.getPlayersList()));
  }

  @ReactMethod
  public void changeInfo(int id, String name, int team, Callback cb) {
    Connection.ChangeInfoRequest req = Connection.ChangeInfoRequest.newBuilder()
      .setId(id)
      .setName(name)
      .setTeam(team)
      .build();
    Connection.GameInfo info = stub.changeInfo(req);

    cb.invoke(convertPlayers(info.getPlayersList()));
  }

  private WritableNativeMap convertPlayers(List<Connection.PlayerInfo> players) {
    WritableNativeMap map = new WritableNativeMap();
    for (Connection.PlayerInfo i : players) {
      WritableNativeMap info = new WritableNativeMap();
      info.putInt("id", i.getId());
      info.putString("name", i.getName());
      info.putInt("team", i.getTeam());

      map.putMap(String.valueOf(i.getId()), info);
    }
    return map;
  }

  @ReactMethod
  public void sendUpdate(double lat, double lon, Callback cb) {
    Connection.MapPosition pos = Connection.MapPosition.newBuilder().setLat(lat).setLon(lon).build();

    Map<Integer, Connection.MapPosition> positions = new HashMap<>();
    List<Connection.PlayerPosition> positionList = stub.updatePosition(pos).getPositionsList();
    for (Connection.PlayerPosition p : positionList) {
      positions.put(p.getId(), p.getPos());
    }
    cb.invoke(positions);
  }

  @Override
  public String getName() {
    return "ConnectionModule";
  }
}
